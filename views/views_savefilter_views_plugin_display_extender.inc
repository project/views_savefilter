<?php

/**
 * @file
 * Defines the View Style Plug-in Extender for Save Filter module.
 */

/**
 * Extends the class views_plugin_display_extender to create a custom form.
 */
// @codingStandardsIgnoreLine
class views_savefilter_views_plugin_display_extender extends views_plugin_display_extender {
  /**
   * Provide a form to edit options for this plug-in.
   */
  // @codingStandardsIgnoreLine
  public function options_definition_alter(&$options) {
    $options['save_filter'] = array(
      'default' => 0,
    );
  }

  /**
   * Provide a form to edit options for this plug-in.
   */
  // @codingStandardsIgnoreLine
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    if ('views_savefilter' === $form_state['section']) {
      $form['save_filter'] = array(
        '#type' => 'checkbox',
        '#title' => t('Save Filter'),
        '#description' => t('Check to save the user filter for this view.'),
        '#default_value' => $this->display->get_option('save_filter'),
      );
    }
  }

  /**
   * Handle any special handling on the validate form.
   */
  // @codingStandardsIgnoreLine
  public function options_submit(&$form, &$form_state) {
    $this->display->set_option('save_filter', $form_state['values']['save_filter']);
  }

  /**
   * Provide the default summary for options in the views UI.
   *
   * This output is returned as an array.
   */
  // @codingStandardsIgnoreLine
  public function options_summary(&$categories, &$options) {
    $options['views_savefilter'] = array(
      'category' => 'other',
      'title' => t('Save Filter'),
      'value' => ($this->display->get_option('save_filter')) ? 'Yes' : 'No',
      'desc' => t('Allows to save the filter for each user that use this view.'),
    );
  }

}
