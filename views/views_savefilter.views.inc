<?php

/**
 * @file
 * Defines the View Style Plug-in for Save Filter module.
 */

/**
 * Implements hook_views_plugins().
 */
function views_savefilter_views_plugins() {
  $path = drupal_get_path('module', 'views_savefilter');
  $plugins = array();

  $plugins['display_extender'] = array(
    'views_savefilter' => array(
      'title' => t('Save Filter'),
      'help' => t('Check to save the user filter for this view.'),
      'path' => $path,
      'handler' => 'views_savefilter_views_plugin_display_extender',
    ),
  );

  return $plugins;
}
