INTRODUCTION
------------

This module provides options to save filter values for views with exposed
filters.This means that if you have a view with exposed filters, the filter
values that the user chooses will be saved and restored the next time the user
returns to the search page provided by the view.


MAINTAINERS
-----------

Current maintainers:
  * Luiz Paulo - https://www.drupal.org/u/luizps
  * Natan Moraes - https://drupal.org/u/natanmoraes

This project has been sponsored by:
  * CI&T - CI&T has more experience than anyone else in leading Drupal
    implementations for multi-brand enterprises. When you need to provide better
    more timely, and relevant web experiences for your customers, CI&T takes
    you where you need to be. (http://drupal.org/node/1530378)


REQUIREMENTS
------------

This module requires the following modules:

  * Views (https://drupal.org/project/views)


INSTALLATION
------------

After enabling the module, go to the View configurations page and check the
"Save Filter" option in the "Advanced-> Other" section. It will enable the
saving of the exposed filter for that display of your views.
User data is saved for each display in the views, so, repeat the
procedure for each view you want to save.
