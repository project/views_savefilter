<?php

/**
 * @file
 * Save filter class.
 */

/**
 * The class used for Views Save Filter.
 */
class ViewsSaveFilterFilter {

  private $uid;
  private $view;
  private $display;

  /**
   * Initializes class variables.
   */
  public function __construct($uid, $view, $display) {
    $this->uid = $uid;
    $this->view = $view;
    $this->display = $display;
  }

  /**
   * Creates a new filter.
   */
  public function save($filter) {
    try {
      db_insert('views_savefilter_filter')
        ->fields(array(
          'uid' => $this->uid,
          'view' => $this->view,
          'view_display' => $this->display,
          'filter' => serialize($filter),
        ))
        ->execute();

      // Making sure the cache really doesn't exist.
      $this->clearCache();
      // Permanent caching the new filter.
      $this->setCache($filter);
    }
    catch (Exception $e) {
      $log = 'The filter for user "@user", view "@view" and display "@display" could not be saved. Error: @error';
      $this->logError($log, $e);
    }
  }

  /**
   * Updates an existing filter.
   */
  public function update($filter) {
    try {
      db_update('views_savefilter_filter')
        ->fields(array(
          'filter' => serialize($filter),
        ))
        ->condition('uid', $this->uid)
        ->condition('view', $this->view)
        ->condition('view_display', $this->display)
        ->execute();

      // Cleaning the old filter cache.
      $this->clearCache();
      // Permanent caching the new filter.
      $this->setCache($filter);
    }
    catch (Exception $e) {
      $log = 'The filter for user "@user", view "@view" and display "@display" could not be updated. Error: @error';
      $this->logError($log, $e);
    }
  }

  /**
   * Gets an existing filter.
   */
  public function get() {
    try {
      $result = db_select('views_savefilter_filter', 'f')
        ->fields('f', array('filter'))
        ->condition('uid', $this->uid)
        ->condition('view', $this->view)
        ->condition('view_display', $this->display)
        ->execute()
        ->fetchField();

      return unserialize($result);
    }
    catch (Exception $e) {
      $log = 'The filter for user "@user", view "@view" and display "@display" could not be deleted. Error: @error';
      $this->logError($log, $e);
    }
  }

  /**
   * Deletes an existing filter.
   */
  public function delete() {
    try {
      db_delete('views_savefilter_filter')
        ->condition('uid', $this->uid)
        ->condition('view', $this->view)
        ->condition('view_display', $this->display)
        ->execute();

      // Cleaning the filter cache.
      $this->clearCache();
    }
    catch (Exception $e) {
      $log = 'The filter for user "@user", view "@view" and display "@display" could not be deleted. Error: @error';
      $this->logError($log, $e);
    }
  }

  /**
   * Checks if filter already exists.
   */
  public function exists() {
    try {
      $result = db_select('views_savefilter_filter', 'f')
        ->fields('f', array('uid'))
        ->condition('uid', $this->uid)
        ->condition('view', $this->view)
        ->condition('view_display', $this->display)
        ->countQuery()
        ->execute()
        ->fetchField();

      if ($result) {
        return TRUE;
      }
    }
    catch (Exception $e) {
      $log = 'An unexpected error occurred for user "@user", view "@view" and display "@display". Error: @error';
      $this->logError($log, $e);
    }
    return FALSE;
  }

  /**
   * Check if cache exists.
   */
  public function isCached() {
    $cache_id = $this->uid . '_' . $this->view . '_' . $this->display;
    if (cache_get($cache_id)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get the cache.
   */
  public function getCache() {
    $cache_id = $this->uid . '_' . $this->view . '_' . $this->display;
    $cache = cache_get($cache_id);
    if (!empty($cache)) {
      return(unserialize($cache->data));
    }
    return FALSE;
  }

  /**
   * Set the cache.
   */
  public function setCache($filter) {
    $cache_id = $this->uid . '_' . $this->view . '_' . $this->display;
    cache_set($cache_id, serialize($filter));
  }

  /**
   * Clear the cache.
   */
  private function clearCache() {
    $cache_id = $this->uid . '_' . $this->view . '_' . $this->display;
    cache_clear_all($cache_id, 'cache', TRUE);
  }

  /**
   * Logs errors for methods.
   */
  private function logError($message, $e) {
    watchdog('views_savefilter', $message, array(
      '@user' => $this->uid,
      '@view' => $this->view,
      '@display' => $this->display,
      '@error' => $e->getMessage(),
    ), WATCHDOG_ERROR);
  }

}
